/**** MODULO per l'IO delle mappe -------------------------------------*/

:- use_module('types/syntax').
:- no_check(write_atom(_,_)).
check_me :- ensure_loaded('types/chk'),
	load([geometry,io_map]).

:-ensure_loaded([aux_io]).

/*----------------------------------------------------------------------*/

%%	TIPI

%type list(T) := []; [T|list(T)].  importato

% 0 ------------------------------------ tipi e predicati aperti

type sym_char. % i simboli che rappresentano i tipi terreno

pred save_dims(number, number, file_name).
%   save_dims(+DX, +DY, +File)  is DET.
%    save(DX,DY,File)  salva in File un predicato
%    contenente le dimensioni DX, DY della mappa

pred save(sym_char, number, number, file_name).
%   save(+C,+X, +Y, +File) is DET.
%    save(C, X, Y, File)  salva in file un predicato
%    che indica la tipologia di terreno dell'area
%    di coordinate (X,Y), codificata dal codice C

%    NOTA IMPORTANTE:  codice -1 = assenza informazioni
%    esplicite, si usera' un tipo di terreno di default



% 1 ----------------------------------- CARICAMENTO MAPPA


%%	read_map(+In, +Out) is det
%       carica in Out la rappresentazione della mappa in In,
%       usando il predicato aperto save

read_map(FileIn, MapFile) :-
	open(FileIn, read, In),
	open(MapFile, write, Out),

	read_dims(In, DX, DY),
	save_dims(DX,DY,Out),

	get_map(1,DX,DY,In, Out),

	close(In),
	close(Out).

get_map(Y, _DX, DY, Fin, MapF) :-
	Y =< DY,
	at_end_of_stream(Fin),
	close(Fin),
	close(MapF),
	throw('UNEXPECTED END OF FILE').

get_map(Y, DX, DY, Fin, MapF) :-
	Y =< DY,
	read_line_to_chars(Fin,Row),
	row_to_map(1, DX, Y, Row, MapF),
	Y1 is Y+1,
	get_map(Y1, DX, DY, Fin, MapF).
get_map(Y, _DX, DY, _FileIn, _MapFile) :-
	Y > DY.

% 3 ---------------------------------------caricamento righe

row_to_map(X,DX,Y,[Code|Row], MapF) :-
	X =< DX,
	save(Code,X,Y,MapF),
	X1 is X+1,
	row_to_map(X1,DX,Y,Row,MapF).
row_to_map(X,DX,Y,[], MapF) :-
	X =< DX,
	save(' ',X,Y,MapF),
	X1 is X+1,
	row_to_map(X1,DX,Y,[],MapF).
row_to_map(X,DX,_,_, _MapF) :-
	X > DX,!.

% 4 ------------------------------------ caricamento dimensioni

read_dims(File, DX, DY) :-
	read_line_to_codes(File,Row),
	writeln(Row),
	to_cifra(Row, RowX),
	get_int(RowX,Next, 0, DX),
	to_cifra(Next,RowY),
	get_int(RowY, _, 0, DY),
	DX > 0,
	DY > 0, !
	;
	throw('manca o e'' errata la riga delle dimensioni').









