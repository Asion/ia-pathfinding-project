:- use_module('types/syntax').

:- consult([
      'Ricerca/astar',
      agenti,
      terreniAttrezzature
   ]).

:- consult(clima).

:- maplist(writeln,[
'\n-------------------------------------------------------\n',
'set_map(<mapfile>,<layoutfile>)    (carica la mappa dal livello nel file mapfile con il layout layoutfile)',
'find_path(<posizione iniziale>, <attrezzatura>)

       il file livello  contiene l''unico livello attualmente creato
       caricato il livello con set_map(livello,attributiLivello), provare con, ad esempio:
       find_path(p2(1,1),[scarpetta]).
       find_path(p2(1,3),[scarpetta, scarpone]).',

'\n-------------------------------------------------------'
		   ]).

% ------------------------------------------ FUNZIONALITA' interfaccia

set_map(Map,Layout) :- 
	load_layout(Layout),
	load_map(Map).

/*
find_path originale, lasciato come commentato per permettere di osservare le differenze tra il codice originale e quello attuale.

find_path(P,A,Sol) :-
	call(set_clima),
	start(P,A,ST),
	nl,
	init_search(ST,F0),
	cerca(F0, Sol),
	nl,
	show_sol(mappa,Sol).
*/

%find_path(+Posizione, +Attrezzature)

find_path(Posizione,Attrezzature) :-
	call(set_clima_iniziale),
	start(Posizione,Attrezzature,Stato),
	search_sol(Posizione,Attrezzature,Stato,0,CostoTotale),
	writeln('Il costo totale è':CostoTotale),
	nl.

%-------------------------------------   Clima
set_clima_iniziale :-
	consult(clima),
	clima_iniziale(Clima),
	clima_definito(Clima,Var),
	nb_setval(clima,Clima).

set_clima(Rand) :-
	Rand > 5,
	nb_getval(clima,Clima),
	clima_definito(Clima,Var),
	cambia_clima(Clima).

set_clima(Rand). %non cambiare clima


print_clima :-
	b_getval(clima,Clima),
	clima_definito(Clima,Var),
	nl,
	writeln('Il clima attuale è':Var),
	nl.

%-------------------------------------    Subgoal

% search_sol(+P2,+Attrezzature,+Tesori,+CostoAttuale,-CostoFinale) esegue l'algoritmo di ricerca tesoro per tesoro fino a quando la lista dei tesori è vuota

search_sol(P2,Attrezzature,[],CostoAttuale,CostoAttuale).

search_sol(P2,Attrezzature,[Tesoro|L],CostoAttuale,CostoFinale) :-

	print_clima,
	init_search(stato(nodir,P2,Attrezzature,[Tesoro]),F0),
	cerca(F0, nc(stato(D,PArrivo,_,_),Path,CostoRicorsione)), ! ,
    
    writeln('Gli oggetti utilizzati sono':Attrezzature),
    nl,

	show_sol(mappa,nc(stato(D,PArrivo,Attrezzature,[]),Path,CostoRicorsione)),

	Rand is random(10) + 1,
    set_clima(Rand),
    modifica_attrezzatura(Attrezzature,NuoveAttrezzature),

	CostoPasso is CostoRicorsione + CostoAttuale,
	search_sol(PArrivo,NuoveAttrezzature,L,CostoPasso,CostoFinale).
	

%-------------------------------------    MOSTRA il path

show_sol(lista,nc(SA,Path,Cost)) :-
	writeln(costo:Cost),
	nl,
	writeln('----------------------------------'),
	reverse([SA|Path], RPath),
	maplist(writeln,RPath),
	writeln('-----------------------------------').

% --------------------------------------  Mostra il percorso come mappa
%
show_sol(mappa,nc(SA, Path, Cost)) :-
	writeln(costo:Cost),
	writeln('-----------mappa------------'),
	dim(DX,DY),
	forall(between(1,DY,Y),
	      (	forall(between(1,DX,X), write_agent(X,Y,[SA|Path])), nl )),
	writeln('----------------------------'),
	nl,
	reverse([SA|Path],RPath),
	maplist(write_pos, RPath),
	nl,
	nl.

write_pos(stato(_,p2(X,Y),_,T)) :-
	length(T,N),
	write('-->'),
	write((X,Y):N).

write_agent(X,Y,L) :-
	member(stato(_, p2(X,Y), _, T),L) ->
	       length(T,N),
	       write(N)
	       ;
	       write(' ').

% -------- sovrascrive taglia_cicli

/*taglia_cicli(stato(_,P,S,T),_) :-
           visited(st(P,S,T)),!
	   ;
	   assert(visited(st(P,S,T))),
	   fail.*/








