
%---------------DOCUMENTATION-------------------------------------
/*************************************************************************
1- Definire layout terreni

Tramite questo file è possibile definire molti tipi di terreno differenti
da utilizzare nelle mappe dei livelli.
Il formato del file è :

tipoTerreno = simbolo , peso

i simboli speciali 'number' e 'space' si traducono in :

number : map(A, monte(N)) :- cifra(A,N), !.

space : map(' ',tipoTerreno).

NB: non è ovviamente possibile definire = simboli terreno o nomi terreno

2-Definire tools e pesi

Ogni tool definito deve avere obbligatoriamente almeno un peso di default stabilito scrivendo :

-tipoTool = peso

peso è un modificatore applicato al peso di default di un terreno e può quindi essere negativo 
o positivo, ovviamente non è possibile definire un peso default più di una volta.
Oltre a i pesi di default è possibile aggiungere un modificatore per un tipo terreno specifico
con il comando:

-tipoTool=terreno,peso

Il terreno non deve essere necessariamente definito nel layout terreni all'inizio del file
 **************************************************************************/

%---------------GENERAL-------------------------------------

:-consult([aux_io]).

load_layout(Layout) :-
    writeln('reading layout file': Layout),
	read_layout(Layout,'l.pl').

read_layout(InputFile,AttFile) :-
    open(InputFile, read, In),
	open(AttFile, write, Out),
    b_setval(toolsDeclared,[]),
	parse_terrain_layout(In,Out),
	close(In),
	close(Out),
    check_for_errors([l]).

parse_terrain_layout(In,Out) :-
	at_end_of_stream(In).

parse_terrain_layout(In,Out) :-
	read_line_to_codes(In,Line),
	parse_line(Line,Out),
	parse_terrain_layout(In,Out).

parse_line(Line,AttFile) :-
	ignore_line(Line).

parse_line(Line,AttFile) :-
	tokenize_atom(Line,Tokens),
	write_values(AttFile,Tokens).

ignore_line([]) :- % discard empty lines
    true.

ignore_line(Line) :- % '%'is a line of comment
    nth0(0,Line,37).
	
%---------------TERRAIN LAYOUT-------------------------------------


write_values(AttFile,[Terrain,'=',number,',',Peso]) :-
    integer(Peso),
    Peso >= 0,

    string_to_atom('(N)',N),
    atom_concat(Terrain,N,TerrainAtom),
    write(AttFile,map(('A'),TerrainAtom)),
    write(AttFile,':-'),
    nl(AttFile),
    write(AttFile,' cifra(A,N), !.'),
    nl(AttFile),

    write(AttFile,peso_terreno(Peso,TerrainAtom)),
    write(AttFile,'.'),
    nl(AttFile),
    b_setval(elevation,Terrain).

write_values(AttFile,[Terrain,'=',space,',',Peso]) :-
    integer(Peso),
    Peso >= 0,
    writeq(AttFile,map(' ',Terrain)),
    write(AttFile,'.'),
    nl(AttFile),
    write(AttFile,peso_terreno(Peso,Terrain)),
    write(AttFile,'.'),
    nl(AttFile).

write_values(AttFile,[Terrain,'=',Symbol,',',Peso]) :-
    integer(Peso),
    Peso >= 0,
    write(AttFile,map(Symbol,Terrain)),
    write(AttFile,'.'),
    nl(AttFile),
    write(AttFile,peso_terreno(Peso,Terrain)),
    write(AttFile,'.'),
    nl(AttFile).

%---------------TOOLS LAYOUT-------------------------------------

%------ELEVAZIONE INIZIO-------------------------------------------

write_values(AttFile,['-',Tools,'=',Terrain,',',Peso]) :-
    integer(Peso),
    b_getval(elevation,TerrainElevation),
    TerrainElevation == Terrain,

    string_to_atom('(_N)',N),
    atom_concat(Terrain,N,TerrainAtom),
    write(AttFile,peso_attrezzatura(Tools,TerrainAtom,Peso)),
    write(AttFile,'.'), 
    nl(AttFile),

    b_getval(toolsDeclared,List),
    append(List,[Tools],NewList),
    b_setval(toolsDeclared,NewList).

%------ELEVAZIONE FINE----------------------------------------
write_values(AttFile,['-',Tools,'=',Terrain,',',Peso]) :-
    integer(Peso),
    write(AttFile,peso_attrezzatura(Tools,Terrain,Peso)),
    write(AttFile,'.'),
    nl(AttFile),

    b_getval(toolsDeclared,List),
    append(List,[Tools],NewList),
    b_setval(toolsDeclared,NewList).

write_values(AttFile,['-',Tools,'=',Peso]) :-
    integer(Peso),
    write(AttFile,peso_attrezzatura_default(Tools,Peso)),
    write(AttFile,'.'),
    nl(AttFile).

%---------------ERROR CHECKING-------------------------------------
write_values(AttFile,InvalidToken) :-
    writeln('invalid token':InvalidToken),
    throw('Invalid format in input map file').

check_for_errors(FileToCheck) :-
    consult(FileToCheck),
    %è necessario che esista un terreno 'tesoro'
    %controlla se i termini map() sono definiti più di una volta con lo stesso terreno
    findall(Terrain,map(Symbol,Terrain),ListTerrains),
    not(is_set(ListTerrains)) -> throw('Errore:un termine terreno è stato definito più volte');
    not(map(S,tesoro)) -> throw('Errore : il tipo terreno tesoro DEVE essere definito');

    %controlla se i termini peso_attrezzatura_default() sono definiti più di una volta con lo stesso attrezzo
    findall(Tools,peso_attrezzatura_default(Tools,Weight),ListTools),
    not(is_set(ListTools)) ->throw('Errore:un termine default attrezzatura è stato definito più volte');
    
    %controlla se tutti i termini peso_attrezzatura_default() sono stati definiti
    findall(Tools,peso_attrezzatura_default(Tools,Weight),ListTools),
    b_getval(toolsDeclared,List),
    list_to_set(List,ToolsSet),
    nb_delete(toolsDeclared),
    not(ToolsSet == ListTools) ->throw('non tutti i necessari predicati peso_attrezzatura_default sono stati definiti');
    
    %controlla se ci sono ridefinizioni in peso_attrezzatura
    findall(Tools,peso_attrezzatura_default(Tools,Weight),ListTools),
    tools_redefiniton(ListTools).

tools_redefiniton([]) :-
    true.

tools_redefiniton([Tool|Tail]) :-
    findall(Terrain,peso_attrezzatura(Tool,Terrain,Weight),ListTerrains),
    not(is_set(ListTerrains)) ->
        writeln('Termine contenente il terreno ridefinito':Tool),
        throw('Errore: un termine attrezzo ridefinisce più volte lo stesso terreno');
    tools_redefiniton(Tail).












