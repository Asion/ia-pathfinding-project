%%	AUSILIARI ------------------------------------------------------

read_line_to_chars(File, Row) :-
	read_line_to_codes(File, CRow),
	maplist(map_to_char, CRow, Row).

map_to_char(Code, Char) :-
	atom_chars(Char,[Code]).

get_int([Code|RowIn], RowOut, IntIn, IntOut) :-
	is_cifra(Code,C) ->
	      NewInt is 10*IntIn+C,
              get_int(RowIn, RowOut, NewInt, IntOut)
	      ;
	      RowOut=[Code|RowIn],
	      IntOut=IntIn.
get_int([],[],I,I).

to_cifra([Code|RowIn], RowOut) :-
	not(is_cifra(Code,_)) ->
              to_cifra(RowIn, RowOut)
	      ;
	      RowOut = [Code|RowIn].

is_cifra(Code,Num) :-
	Num is Code-48,
	Num >= 0,
	Num < 10.