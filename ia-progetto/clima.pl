/************************************************************************************************************
							                CLIMA
Il clima è rappresentato da un valore intero > 0 e contenuto in una variabile globale "clima". 

Per aggiungere un clima è necessario definire il predicato clima_definito(NumeroClima,ID) con il numero del clima e 
una stringa che lo identifica. Per esempio :

clima_definito(1,'Estate'). assegna al clima 1 l'identificatore "estate"

Per ogni clima N devono essere necessariamente definiti due predicati :

-peso_clima(Clima,PesoClima) il modificatore su terreni di default, viene applicato nel calcolo del peso dei 
terreni per i quali non è stato definito un predicato specifico.

-peso_clima_attrezzatura_default(Clima,PesoAttrezzatura) il modificatore su attrezzature di default, viene applicato
nel calcolo dei modificatori attrezzatura per i quali non è stato definito un peso specifico

E' possibile inserire predicati per sovrascrivere il calcolo del modificatore clima per un terreno specifico definendo
il predicato peso_clima_terreno(Terreno,Clima,PesoC) dove "Terreno" è il terreno specifico che stiamo utilizzando 
e Clima l'identificatore  del clima, Per esempio :

-peso_clima_terreno(monti(_N),1,5) indica che il modificatore 5 verra aggiunto al peso del terreno "monti" nel caso 
la variabile clima corrisponda a 1.

Lo stesso processo può essere utilizzato per le attrezzature definendo il predicato:

-peso_clima_attrezzatura(Clima,Calzatura,Peso).'

E' necessario specificare un clima iniziale attraverso il predicato 

clima_iniziale(ClimaIniziale).

Dove "ClimaIniziale" è un clima_definito

Durante il cambiamento di clima viene invocato il predicato:

cambia_clima(ClimaAttualeDaModificare)

Questo predicato modifica il valore del clima corrente.
"ClimaAttualeDaModificare" è il valore corrente della variabile globale "clima", è necessario che il predicato possa unificare 
con tutti i valori assumibili da questa variabile (cioè tutti i clima_definito).


**************************************************************************************************************/
%supponiamo due tipi di clima 1 == estate, 2 == inverno.

clima_iniziale(1).

clima_definito(1,'Estate').

clima_definito(2,'Inverno').

%chiamato ogni volta che è trovata un tesoro modifica l'attrezzatura dell'NPC da Attrezzatura a NuovaAttrezzatura

modifica_attrezzatura(Attrezzatura,NuovaAttrezzatura) :-
	nb_getval(clima,Clima),
	Clima = 2,
	not(member(sci,Attrezzatura)),
	append(Attrezzatura,[sci],NuovaAttrezzatura).

modifica_attrezzatura(Attrezzatura,NuovaAttrezzatura) :-
	NuovaAttrezzatura = Attrezzatura.

% cambia il clima

cambia_clima(ClimaVal) :-
	ClimaVal = 1,
	b_setval(clima,2).

cambia_clima(ClimaVal) :-
	ClimaVal = 2,
	b_setval(clima,1).

%clima default terreni

peso_clima(1, 0).

peso_clima(2,+3).

%clima tereni specifici

%inverno

peso_clima_terreno(2,monte(_N),+3).

peso_clima_terreno(2,palude,-3).

peso_clima_terreno(2,bosco,+4).

%estate

peso_clima_terreno(1,palude,+3).

peso_clima_terreno(1,monte(_N),-1).

%Clima default attrezzature

peso_clima_attrezzatura_default(1,0).

peso_clima_attrezzatura_default(2,+2).

%Clima specifico per attrezzature

%scarpone

peso_clima_attrezzatura(2,scarpone,-1).

peso_clima_attrezzatura(1,scarpone,0).

%scarpetta

peso_clima_attrezzatura(2,scarpetta,+2).

peso_clima_attrezzatura(1,scarpetta,-1).

%sci

peso_clima_attrezzatura(2,sci,-3).

peso_clima_attrezzatura(1,sci,+20).
