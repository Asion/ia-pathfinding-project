%================================================
%               MESSAGGISTICA DI HELP
%               M.O.  08
%===============================================
:- module(gen_msg, [hh/0, hh/1]).

hh :-
writeln(
'HELP.      Per vedere i comandi di generazione, usare:
---> hh	         questo stesso help
---> hh(load)	 comandi per caricare i programmi su cui fare type checking
---> hh(sign)    comandi per rivedere la segnatura
---> hh(terms)   comandi per la generazione di termini
---> hh(atomic)  comandi per la generazione di atomiche
---> hh(prog)	 comandi per la parte di generazione relativa al programma'),nl.

hh(load) :-
nl, writeln(
'Controllo e ricostruzione tipi. La segnatura ricostruita e'' salvata
in "...outfile.pl" ispezionabile direttamente o con i comandi mosrati
dall''help in linea fornito dal comando hh(X).
--> load(<in>).		         carica <in>, l''outfile ha nome <in>_out.pl
    load([<in1>,...,<ink>]).     carica k files, l''outfile ha nome out.pl
    load(<in>,  <out>).          carica <in>, l''outfile ha nome <out>.pl
    load([<in1>,..,<ink>],<out>). carica k files con outfile <out>.pl

    hh, hh(X) per help
--------------------------------------------------------------------------').

hh(sign) :-
nl,writeln('SEGNATURA:'),nl,
writeln(
'---> is_type(?T:term).        enumera le dichiarazioni di tipo
---> is_fun(?F:term).         enumera le dichiarazioni di funzione
---> is_pred(?P:pred).        enumera le dichiarazioni di predicato'),nl,
writeln('*** digita ; per continuare; per altri help:  crh e genh').

% -------------------------------------------------------------------

hh(terms) :-
nl,writeln('TERMINI:'),nl,
writeln(
'---> is_of(+T:Term, -Ty:Type).	         trova il tipo Ty di T'),
writeln(
'---> is_term(-T:Term, ?Ty:Type, +H:Int).  elenca i T di tipo Ty
					 e altezza <= H'),
writeln(
'---> g_term(?T:Term, ?Ty:Type, +H:Int).  elenca i T ground di tipo Ty
                                         e altezza <= H').

% ------------------------------------------------------------------

hh(atomic) :-
nl,writeln('ATOMICHE:'),nl,
%writeln(
%'---> is_atomic(+A:term).	  A e'' un''atomica'),
writeln(
'---> g_atomic(?A:term, +H:int).   elenca le atomiche A con termini
				  di altezza <=H').

% --------------------------------------------------------------------

hh(prog) :-
nl,writeln('PROGRAMMA:'),nl,
writeln(
'---> is_clause(?H:term, ?B:body).   H :- B e'' una clausola
     is_warning(?A,?B).              mostra i warnings').

% --------------------------------------------------------------------














