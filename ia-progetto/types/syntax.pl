:- module(syntax, [
		  op(1199, fx, type),
		  op(1199, fx, pred),
		  op(1110, xfy, (:=)),
		  no_check/1,
		  check/1]).

no_check(_).
check(_).
