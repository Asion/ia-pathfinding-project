:- module(utils, [unifies_term/4,
		  normalize_vars/2,
		  normalize_vars/3,
		  normalize_vars/4]).


unifies_term(E1,E2,Functor,Args) :-
	unify_with_occurs_check(E1,E2),
	E1 =.. [Functor|Args].


%%	to replace internally renamed variables _5643 by X1,X2,...
normalize_vars(V,C) :-
	term_variables(C,Vars),
	normalize_vars(1,V,C,Vars).

normalize_vars(V,C,Vars) :-
	term_variables(C,Vars),
	normalize_vars(1,V,C,Vars).

normalize_vars(_,_,_,[]).
normalize_vars(N,Sym,C,[V|W]) :-
	concat(Sym,N,XN),
	V = XN,
	M is N+1,
	normalize_vars(M,Sym,C,W).

