/***********************************************************
  Contiene sia la base di conoscenza degli agenti possibili (i pesi dei
  vari tipi di terreno, la geometria (con la consult(mappe) che
  include anche geometry e l'io_map) e il caricamento della mappa del
  livello con load_map
  */
:- consult(mappe).    %% Usa la KB delle mappe
:- use_module('types/syntax').
:- no_check(findall(_,_,_)).
check_me :- ensure_loaded('types/chk'),
	load([geometry, io_map, mappe, agenti]).
/******************************************************************************/


% 0. ----------------------------------------------------KB TIPI

type calzatura := scarpone; scarpetta.

type stati := stato(direzione, punto2, calzatura, list(punto2)).

type fn := m ; 'm.pl'.  %file names

% 1. ----------------------------------------------------KB  mappe
%                                deve essere caricata la mappa

load_map(Livello) :-
	read_map(Livello, 'm.pl'),
	consult(m),
	show_map,
	!. %evita inutili tentativi di backtraking


% 2.0  ---------------------------------------------------KB  PESI
%
/*
peso(bosco,scarpone,1).
peso(bosco,scarpetta,1).

peso(palude, scarpone, 6).
peso(palude, scarpetta, 6).

peso(monte(_N), scarpone, 1).
peso(monte(_N), scarpetta, 5).

peso(piano,scarpone,1.5).
peso(piano, scarpetta, 1).

peso(sabbia,scarpone,3).
peso(sabbia,scarpetta,2.5).

peso(tesoro,_,0).
*/

% 2.1  -----------------------------------  KB geometrica: i dislivelli
%				  positivi (salita), negativi(discesa)

dislivello(monte(N1),monte(N2),H) :- !,
	H is N2-N1.
dislivello(monte(N),_T,-N) :- !.

dislivello(_T,monte(N),N) :- !.

dislivello(_,_,0).

% 2.1 -------------------------------------------KB  MOTO e tempi

%%	movimento(+P1,?P2,?Dir)	 nondet:  P2 e' adiacente a P1
%       con direzione Dir
movimento(P1,P2,Dir) :-
	rosa_venti(Dir),
	spostamento(P1,P2,Dir),
	ok(P2).

%%	i cambi direzione costano 0.1, non costa nulle
%       mantenere la direzione
rotazione(D,D,0) :- !.
rotazione(_,_,0.1).

%%	tempo_base(+P1, +P2, +H, -T)  tempo dipendente dalle
%	distanze e dal dislivello, indipendentemente dalla
%	specifica attrezzatura; l'ipotesi e' il tempo
%	sia il tempo base moltiplicato per il "peso" del tipo
%	di terreno con quell'attrezzatura
tempo_base(P1, P2, H, T) :-
	distanza(P1,P2,D),
	(   H >= 0 ->
	          T is 4*H+D
	          ;
	          T is D - 2*H ).


% 3. ---------------------------------------------- EURISTCA

%%	start(+P,+A,-S):  S e' il nodo di partenza
%       con posizione P e attrezzatura A; deve prendere tutti i
%       tesori
start(P,A,Tesori) :-
      findall(P1, terreno(tesoro,P1), Tesori).

%%	calcolo dei vicini

vicino(stato(_Dir, P,Scarpa,Tesori), stato(Dir1, P1,Scarpa,Tesori1)):-
	member(P1,Tesori),
	movimento(P,P1,Dir1),!,
	subtract(Tesori,[P1],Tesori1).
vicino(stato(_Dir, P,Scarpa,T), stato(Dir1, P1, Scarpa, T)) :-
	movimento(P,P1,Dir1).


vicini(S,L) :-
	findall(X,vicino(S,X),L).

%%	i nodi goal: ha preso tutti i tesori

trovato(stato(_,_,_,[])).

%% peso

%minimo tra due interi
is_min(L,R,L) :-
	L < R.

is_min(L,R,R) :-
	L >= R.

%calcola peso terreno utilizzando il clima attuale, predicati con *_clima_* sono aperti

calcola_peso_terreno(Terreno,Peso) :-
	nb_getval(clima,Clima),
	peso_clima_terreno(Terreno,Clima,PesoC),
	peso_terreno(PesoT,Terreno),
	Peso is PesoT + PesoC.

calcola_peso_terreno(Terreno,Peso) :-
	nb_getval(clima,Clima),
	peso_clima(Clima,PesoC),
	peso_terreno(PesoT,Terreno),
	Peso is PesoT + PesoC.

%calcola peso attrezzatura utilizzando il clima attuale, predicati con *_clima_* sono aperti

calcola_peso_attrezzatura_clima(Calzatura,Peso) :-
	nb_getval(clima,Clima),
	peso_clima_attrezzatura(Clima,Calzatura,Peso).

calcola_peso_attrezzatura_clima(Calzatura,Peso) :-
	nb_getval(clima,Clima),
	peso_clima_attrezzatura_default(Clima,Peso).

	
%calcola il peso di una coppia terreno attrezzatura
calcola_peso_attrezzatura(Terreno,Calzatura,Peso) :-
	peso_attrezzatura(Calzatura,Terreno,PesoAttrezzatura),
	calcola_peso_attrezzatura_clima(Calzatura,PesoAttrezzaturaC),
	calcola_peso_terreno(Terreno,PesoTerreno),
	Peso is PesoTerreno + (PesoAttrezzatura + PesoAttrezzaturaC).

calcola_peso_attrezzatura(Terreno,Calzatura,Peso) :-
	peso_attrezzatura_default(Calzatura,PesoAttrezzatura),
	calcola_peso_attrezzatura_clima(Calzatura,PesoAttrezzaturaC),
	calcola_peso_terreno(Terreno,PesoTerreno),
	Peso is PesoTerreno + (PesoAttrezzatura + PesoAttrezzaturaC).

%% calcolo attrezzo con peso migliore

%caso base

calcola_peso(Terreno,[],PesoIn,PesoIn).

%parte ricorsiva

calcola_peso(Terreno,[Calzatura|Coda],PesoIn,PesoOut) :-
	%nl,
	%writeln(terreno:Terreno),
	%writeln(pesoIn:PesoIn),
	%writeln(calzatura:Calzatura),
	calcola_peso_attrezzatura(Terreno,Calzatura,Peso), 
	%writeln('peso attrezzatura':Peso),
	%nl,
	is_min(Peso,PesoIn,PesoMin),
	calcola_peso(Terreno,Coda,PesoMin,PesoOut),!.

%inizia il calcolo dei pesi sulla lista attrezzatura, necessaria per avere un termine ground in PesoAttuale
calcola_peso_migliore(Terreno,[Calzatura|Coda],PesoFinale) :-
	%writeln('----------INIZIO SCELTA---------------'),
	%writeln('Attrezzature disponibili':[Calzatura|Coda]),
	calcola_peso_attrezzatura(Terreno,Calzatura,Peso),
	%writeln(terreno:Terreno),
	%writeln(calzatura:Calzatura),
	%writeln('peso attrezzatura':Peso),
	%inizia la ricorsione
	calcola_peso(Terreno,Coda,Peso,PesoFinale).
	%nl,
	%writeln('Peso migliore':PesoFinale),
	%writeln('----------FINE SCELTA---------------').

%%	i costi

% a) prendere il tesoro in un passo non costa niente
costo(stato(_Dir1,_P1,Scarpa,Tesori1), stato(_Dir2,P2,Scarpa,Tesori2), 0) :-
	member(P2,Tesori1),
	subtract(Tesori1,[P2],Tesori2).

% b)  il costo dipende dalla distanza, dal dislivello, dal tipo
%     di terreno e dall'attrezzatura utilizzata. L'agente usa sempre l'attrezzatura con il costo minore.
costo(stato(Dir1,P1,Scarpa,Tesori1), stato(Dir2,P2,Scarpa,Tesori1), Costo) :-
	not(member(P2,Tesori1)),
    % a. Tipologia terreno e peso
	terreno(T1,P1),
	terreno(T2,P2),

	calcola_peso_migliore(T1,Scarpa,Peso1),
	calcola_peso_migliore(T2,Scarpa,Peso2),
    PesoMedio is (Peso1+Peso2)/2,

    % b. tempo base
	dislivello(T1,T2,H),
	tempo_base(P1,P2,H,TBase), % tempo base
    % c. tempo rotazione
	rotazione(Dir1,Dir2,R), % tempo rotazione
    % d. tempo complessivo
	Costo is TBase*PesoMedio + R.


%h(_,0).
h(stato(_,P1,_A,L), H) :-
	max_dist(P1,L,H).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   Predicati ausiliari
max_dist(_,[],-1).
max_dist(P,[P1|L],H) :-
	%distanza_griglia(P,P1,H1),
	distanza(P,P1,H1),
	max_dist(P,L,H2),
	max(H1,H2,H).

max(X,Y,Y) :- X =< Y.
max(X,Y,X) :- X > Y.


gen_stato(S, stato(dir(1,1),P,S,T)) :-
	tesori(T),
	ok(P).
