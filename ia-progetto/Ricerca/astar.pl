:- use_module('../types/syntax').
:- consult(frontiera_ord).
:- consult(search).
:- consult(dfi).

:- opening('A*').

%%%  STRATEGIA A*
%%   USA FRONTIERA ORDINATA, implementata come priority queue


%%	h(+Nodo, -H) is det:   H = h(N)
%       euristica definita dal problema
pred h(_Nodo, number).    %% APERTO


%  COMMENTARE l'attivazione/ disattivazione o usare una vostra
%  implementazione

%taglia_cicli(_,_) :- fail.              %% TAGLIO DISATTIVATO
%taglia_cicli(N,L) :- member(N,L).	%% TAGLIO CICLI ATTIVATO
%/*                                      %% TAGLIO NODI ATTIVATO
taglia_cicli(N,_) :-
           visited(N),!
	   ;
	   assert(visited(N)),
	   fail.
%*/

%%  leq(+N1, +N2) is semidet:  ordinamento della frontiera da usare in
%%  frontiera_ord.pl
%%  per A* la frontiera e' ordinata in base a f(N) = g(N) + h(N)
%
leq(nc(N1,_,Costo1),nc(N2,_,Costo2)) :-
       h(N1, W1),
       h(N2, W2),
       Costo1+W1 =< Costo2+W2.

















