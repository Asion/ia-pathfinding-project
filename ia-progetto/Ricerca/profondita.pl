:- use_module('../types/syntax').
:- consult(search).
:- consult(frontiera_list).
:- consult(dfi).
:- opening('PROFONDITA''').

%  COMMENTARE l'attivazione/ disattivazione o usare una vostra
%  implementazione

taglia_cicli(_,_) :- fail.              %% TAGLIO DISATTIVATO
%taglia_cicli(N,L) :- member(N,L).	%% TAGLIO CICLI ATTIVATO
/*                                      %% TAGLIO NODI ATTIVATO
taglia_cicli(N,_) :-
           visited(N),!
	   ;
	   assert(visited(N)),
	   fail.
*/
