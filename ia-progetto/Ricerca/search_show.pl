/***   Showing the solutions and tracing *******/

/* A)   Predicates that CAN BE OVERRIDEN ****/

write_frontier_node(nc(N, Path, Cost)) :-
	(   showpath -> write_path(nc(N, Path, Cost)) ; true),
	writeln('NODE':N).
write_problem_node(N) :-
	write(N).

write_nc(nc(N, Path, Cost)) :-
        write_path(nc(N, Path, Cost)),
	writeln('SOLUTION':N).

write_path(nc(_N, Path, _Cost)) :-
	reverse(Path,RPath),
	writeln('PATH:'),
	maplist(write_arc, RPath).

write_arc(N) :-
	write_problem_node(N),
	writeln('   -->').

write_frontier(Frontiera) :-
	mostra(Frontiera),!, % implementato dalla frontiera
	command
	;
	throw(errore_mostra_frontiera('rivedere i predicati sovrascritti')).


/* C) ******** Showing and Tracing commands **************/

:- dynamic(showflag/0).
:- dynamic(showpath/0).

hf :- maplist(writeln,[
		       'sf.   (show frontier, mostra la frontiere)',
		       'nf.   (non mostrare la frontiera)',
		       'sp.   (show path, mostra i cammini)',
		       'np.   (non mostrare i cammini)',
		       't.    (Prolog trace)',
		       'n.    (Prolog notrace)',
		       'a.    (Prolog abort)',
		       'hf.   (help traccia frontiera)'
		      ]).

sf :- assert(showflag), writeln('show frontier on').
nf :- retractall(showflag), writeln('show frontier off').
sp :- assert(showpath), writeln('show path on').
np :- retractall(showpath), writeln('show path off').

command :-
	readln(R),
	(R=[C|_] ->
	    (	C=t -> trace, writeln('trace on');
	        C=n -> notrace, writeln('trace off');
	        C=a -> abort;
	        C=nf -> nf;
	        C=sp -> sp;
	        C=np -> np;
	       (  C=hf ; C=h ) ->
	               hf,
		       writeln('<Ret> per continuare'),
	               command
	          ;
		  writeln('COMANDO IGNOTO'),
	          hf,
	          writeln('<Ret> per continuare'),
	          command
	    ))
	;
	true.



