:- consult(frontiera_ord).
:- consult(search).
:- consult(dfi).
:- opening('COSTO MINIMO').
check_me :-
	ensure_loaded('../types/chk'),
	load([search, costo_minimo], 'out.pl').

%%%  STRATEGIA costo minimo (lowest cost first), detta anche
%    costo uniforme, ordinamento frontiera_ord in base al costo
%
leq(nc(_,_,Costo1),nc(_,_,Costo2)) :-
       Costo1 =< Costo2.

%%   USA FRONTIERA ORDINATA, implementata come priority queue

%  COMMENTARE l'attivazione/ disattivazione o usare una vostra
%  implementazione

taglia_cicli(_,_) :- fail.              %% TAGLIO DISATTIVATO
%taglia_cicli(N,L) :- member(N,L).	%% TAGLIO CICLI ATTIVATO
/*                                      %% TAGLIO NODI ATTIVATO
taglia_cicli(N,_) :-
           visited(N),!
	   ;
	   assert(visited(N)),
	   fail.
*/

