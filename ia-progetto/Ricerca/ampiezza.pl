:- use_module('../types/syntax').
:- consult('search').
:- consult(frontiera_difflist).
:- consult(dfi).
:- opening('AMPIEZZA').
check_me :-
	ensure_loaded('../types/chk'),
	load([search, frontiera_difflist, ampiezza], 'out.pl').




%  COMMENTARE l'attivazione/ disattivazione o usare una vostra
%  implementazione

%taglia_cicli(_,_) :- fail.              %% TAGLIO DISATTIVATO
taglia_cicli(N,L) :- member(N,L).	%% TAGLIO CICLI ATTIVATO
/*                                      %% TAGLIO NODI ATTIVATO
taglia_cicli(N,_) :-
           visited(N),!
	   ;
	   assert(visited(N)),
	   fail.
*/






