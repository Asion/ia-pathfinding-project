:- use_module('../types/syntax').
check_me :- ensure_loaded('../types/chk'),
	load([search,frontiera_difflist]).

% --------------------------------------------------------------------
% frontiera reallizzata come una difference list (dl) per appendere in
% fondo


type frontiera(TN) := dl(list(TN),list(TN)).

frontiera_iniziale(N,dl([N|L],L)).

scelto(N,dl(U,V),dl(UU,V)) :-
	not(var(U)),
	U=[N|UU].

aggiunta(N,dl(F,G),dl(F,NF)) :-
	to_dif_list(N,dl(FN,LN)),
	concat_dl(dl(F,G), dl(FN,LN), dl(F,NF)).

to_dif_list([],dl(F,F)).
to_dif_list([X|R],dl([X|F],G)) :-
	to_dif_list(R,dl(F,G)).

concat_dl(dl(A,B),dl(B,C),dl(A,C)).


mostra(dl(L1,_)) :-
	var(L1), !.
mostra(dl([N|L1],L2)) :-
	write_frontier_node(N),     %definito in interface.pl
	mostra(dl(L1,L2)).
