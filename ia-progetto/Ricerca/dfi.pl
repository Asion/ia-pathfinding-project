/************* DEFAULT INTERFACE **************/

opening(Strategy) :-
	maplist(writeln,[
			 '--------------------------',
			 Strategy,
			 '--------------------------']),
	opening_commands,
	writeln('-----------------------------------------------------------------'),
	nl.
opening_commands :- maplist(writeln,[
'change_problem(<file problema>).        (cambio problema)',
'change_strategy(<nome strategia>).      (cambio strategia)',
'print_sol(<+ starting node>, < -Cost>)  (lancio)\n',
'HELP:',
'hh.         (help comandi visualizzazione frontiera)',
'man.        (manualetto)',
'strategie.  (lista strategie implementate)'
			]).
strategie :-
	writeln('strategie: ampiezza, astar, best_first, costo_minimo, profondita').

man :- nl,
       opening_commands,nl,
       strategie,
       writeln('per tracciare la frontiera o vedere i cammini:'),
       hh,
       nl,
       writeln('***NB.  Taglio nodi: intervenire sul codice delle strategie'). % from search_show.pl


/* B) Default interface ****************************/

change_problem(Problem) :-
	consult(Problem).

change_strategy(Str) :-
	consult(Str).

print_sol(StartingNode, Cost) :-
	solve(StartingNode, nc(N,P,Cost)),  % from search.pl
	write_nc(nc(N,P,Cost)).		    % from search_show.pl

