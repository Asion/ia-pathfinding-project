:- consult(frontiera_ord).
:- consult(search).
:- consult(dfi).
:- opening('BEST FIRST').

%%%  STRATEGIA BEST FIRST
%%   USA FRONTIERA ORDINATA, implementata come priority queue

%  COMMENTARE l'attivazione/ disattivazione o usare una vostra
%  implementazione

taglia_cicli(_,_) :- fail.              %% TAGLIO DISATTIVATO
%taglia_cicli(N,L) :- member(N,L).	%% TAGLIO CICLI ATTIVATO
/*                                      %% TAGLIO NODI ATTIVATO
taglia_cicli(N,_) :-
           visited(N),!
	   ;
	   assert(visited(N)),
	   fail.
*/

%% best first:	ordinamento frontiera_ord.pl in base ad h(N)
%
leq(nc(N1,_,_),nc(N2,_,_)) :-
       h(N1, W1),
       h(N2, W2),
       W1 =< W2.
