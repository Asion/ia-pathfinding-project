:- use_module('../types/syntax').
check_me :- ensure_loaded('../types/chk'),
	load([search, frontiera_list]).

type frontiera(TN) := fl(list(TN)).

frontiera_iniziale(N,fl([N])).

scelto(N,fl([N|F]),fl(F)).

aggiunta(N,fl(F),fl(NF)) :- append(N,F,NF).

mostra(fl(L)) :-
	maplist(write_frontier_node,L).  %write_nc definito in interface.pl
