/***********************************************************
     Definisce i tipi di terreno  e le mappe
***********************************************************/
:- use_module('types/syntax').
:- consult(geometry).
:- consult(io_map).
:- no_check(write_atom(_,_)).
:- no_check(cifra(_,_)).
check_me :- ensure_loaded('types/chk'),
	load([geometry, io_map, mappe]).


% 0. ---------------------------------------------------- TIPI
%
type qualita := bosco; palude; monte(number); piano; sabbia; tesoro.

type sym_char := b; p; s; ' '; t.

type terreno := terreno(qualita, number, number).

% 1. Realizza i predicati richiesti dal modulo io_map.pl

%%	una casella (X,Y) con simbolo di qualita' QS  viene
%	rappresentata nel file mappa dal predicato
%	terreno(Q, p2(X,Y)), dove Q e' la qualita' di terreno
%	rappresentata dal carattere QS
save(QS,X,Y,File) :-
	map(QS,Q),
	write_atom(File, terreno(Q,p2(X,Y))).

%%	le dimensioni DX e DY sono salvate nel predicato
%	dim(DX, DY)
save_dims(DX,DY,File) :-
	write_atom(File, dim(DX,DY)).


%%	ausiiari

%posizioni nell'area della mappa
ok(p2(X,Y)) :-
	dim(DX,DY),
	between(1,DX,X),
	between(1,DY,Y).

%%	scrive in F un fatto terminato da .
write_atom(F,A) :-
	writeq(F,A),
	write(F,'.'),
	nl(F).

% 2. ------------------------------- Corrispondenza fra i tipi di
%                                   terreno e i caratteri usati per
%                                   rappresentarli


/*
map(A, monte(N)) :-
	cifra(A,N), !.
map(p, palude).
map(b,bosco).
map(s,sabbia).
map(t, tesoro).
map(' ', piano).
*/

%%	cifra(+C,-N) DET:   C e' una cifra decimale e
%                           N e' il suo valore numerico
cifra(C,N) :-
	catch(atom_number(C,N),
	    _E,
	    fail).

% 3. ------------------------------------------- I TESORI

tesori(Tesori) :-
	findall(P, terreno(tesoro,P), Tesori).


/*****************************  VISUALIZZAZIONE MAPPA **************/

%%	mostra sull'output corrente la mappa corrente
%       cioe' quella attualmente caricata
%       PRECONDIZIONE:  consult(Mappa) gia' eseguito
show_map :-
	dim(DX,DY),
	forall(between(1,DY,Y),
	       ( forall(between(1,DX,X), write_terreno(X,Y)), nl )).


write_terreno(X,Y) :-
	terreno(QT,p2(X,Y)),
	map(S,QT),
	write(S).







