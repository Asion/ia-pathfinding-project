/*  Geometria  e  mappe (in casi meno semplificati conviene dividere
    geometria da mappe ed elaborate le mappe in mappe di influenza */

:- use_module('types/syntax').
check_me :- ensure_loaded('types/chk'),
	load([geometry]).

type list(T) := []; [T|list(T)].

type punto2 := p2(number,number).

type direzione := dir(number,number); nodir.

/******  GEOMETRIA  *******************/

%%	spostamento dato dal vettore dir(DX,DY)
spostamento(p2(X,Y),P2,dir(DX,DY)) :-
          var(P2) ->         % modo spostamento(+,-,+)
              X2 is X+DX,
	      Y2 is Y+DY,
	      P2 = p2(X2,Y2)
	      ;
	      P2=p2(X2,Y2),  % modo spostamento(+,+,-)
	      DX is X2-X,
	      DY is Y2-Y.

%%	distanza euclidea
distanza(P1,P2,D) :-
	spostamento(P1,P2,dir(DX,DY)),
	D is sqrt(DX**2 + DY**2).

% direzioni:  dir(0,-1) nord, dir(0,1) sud, dir(-1,0) ovest,
%             dir(1,0) est e le somme danno le direzioni miste
%             ad es: nord-est = dir(0,-1)+dir(1,0) = dir(1,-1)
rosa_venti(dir(DX,DY)) :-
	member(DY,[1,0,-1]),
	member(DX,[-1,0,1]),
	not((DX = 0, DY = 0)).

%%	distanza minima con il vincolo di movimento su
%       diagonali, orizzontale, verticale nella griglia
%
distanza_griglia(p2(X1,Y1), p2(X2,Y2), D) :-
	abs(X1-X2) < abs(Y1-Y2) ->
	        D is abs(Y1-Y2) + 0.41*abs(X1-X2)
		;
		D is abs(X1-X2) + 0.41*abs(Y1-Y2).








